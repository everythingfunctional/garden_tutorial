module stack_test
    use garden, only: &
            input_t, &
            result_t, &
            test_item_t, &
            transformation_failure_t, &
            transformed_t, &
            assert_equals, &
            assert_that, &
            fail, &
            given, &
            it_, &
            then__, &
            when
    use stack_m, only: stack_t
    use stack_input_m, only: stack_input_t

    implicit none
    private
    public :: test_stack
contains
    function test_stack() result(tests)
        type(test_item_t) :: tests

        tests = given( &
                "a new stack", &
                stack_input_t(stack_t()), &
                [ it_("it is empty", check_empty) &
                , it_("it has a depth of zero", check_empty_depth) &
                , when( &
                        "an item is pushed onto it", &
                        push_item, &
                        [ then__("it is no longer empty", check_not_empty) &
                        , then__("it has a depth of one", check_depth_one) &
                        ]) &
                ])
    end function

    function check_empty(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(stack_t) :: stack

        select type (input)
        type is (stack_input_t)
            stack = input%stack()
            result_ = assert_that(stack%empty())
        class default
            result_ = fail("expected to get a stack_input_t")
        end select
    end function

    function check_empty_depth(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(stack_t) :: stack

        select type (input)
        type is (stack_input_t)
            stack = input%stack()
            result_ = assert_equals(0, stack%depth())
        class default
            result_ = fail("expected to get a stack_input_t")
        end select
    end function

    function push_item(input) result(output)
        class(input_t), intent(in) :: input
        type(transformed_t) :: output

        type(stack_t) :: stack

        select type (input)
        type is (stack_input_t)
            stack = input%stack()
            output = transformed_t(stack_input_t(stack%push(1)))
        class default
            output = transformed_t(transformation_failure_t(fail( &
                    "expected to get a stack_input_t")))
        end select
    end function

    function check_not_empty(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(stack_t) :: stack

        select type (input)
        type is (stack_input_t)
            stack = input%stack()
            result_ = assert_that(.not.stack%empty())
        class default
            result_ = fail("expected to get a stack_input_t")
        end select
    end function

    function check_depth_one(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(stack_t) :: stack

        select type (input)
        type is (stack_input_t)
            stack = input%stack()
            result_ = assert_equals(1, stack%depth())
        class default
            result_ = fail("expected to get a stack_input_t")
        end select
    end function
end module
